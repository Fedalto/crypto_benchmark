#include <unistd.h>
#include <stdint.h>

// Libgcrypt
#include <gcrypt.h>

#include "key_length.h"


// AES
void gcrypt_encrypt_aes (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key) {
  gcry_cipher_hd_t g_handle;
  gcry_cipher_open(&g_handle, GCRY_CIPHER_AES256, GCRY_CIPHER_MODE_ECB, 0);
  gcry_cipher_setkey(g_handle, key, aes_key_length);
  gcry_cipher_encrypt(g_handle, output_b, buffer_length, input_b, buffer_length);
  gcry_cipher_close(g_handle);
}

void gcrypt_decrypt_aes (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key) {
  gcry_cipher_hd_t g_handle;
  gcry_cipher_open(&g_handle, GCRY_CIPHER_AES256, GCRY_CIPHER_MODE_ECB, 0);
  gcry_cipher_setkey(g_handle, key, aes_key_length);
  gcry_cipher_decrypt(g_handle, output_b, buffer_length, input_b, buffer_length);
  gcry_cipher_close(g_handle);
}


// DES
void gcrypt_encrypt_des (uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key) {
  gcry_cipher_hd_t g_handle;
  gcry_cipher_open(&g_handle, GCRY_CIPHER_DES, GCRY_CIPHER_MODE_ECB, 0);
  gcry_cipher_setkey(g_handle, key, des_key_length);
  gcry_cipher_encrypt(g_handle, output_b, buffer_length, input_b, buffer_length);
  gcry_cipher_close(g_handle);
}

void gcrypt_decrypt_des (uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key) {
  gcry_cipher_hd_t g_handle;
  gcry_cipher_open(&g_handle, GCRY_CIPHER_DES, GCRY_CIPHER_MODE_ECB, 0);
  gcry_cipher_setkey(g_handle, key, des_key_length);
  gcry_cipher_decrypt(g_handle, output_b, buffer_length, input_b, buffer_length);
  gcry_cipher_close(g_handle);
}


// 3DES
void gcrypt_encrypt_3des (uint8_t *input_b, uint8_t *output_b,
                           unsigned int buffer_length, uint8_t *key) {
  gcry_cipher_hd_t g_handle;
  gcry_cipher_open(&g_handle, GCRY_CIPHER_3DES, GCRY_CIPHER_MODE_ECB, 0);
  gcry_cipher_setkey(g_handle, key, des3_key_length);
  gcry_cipher_encrypt(g_handle, output_b, buffer_length, input_b, buffer_length);
  gcry_cipher_close(g_handle);
}

void gcrypt_decrypt_3des (uint8_t *input_b, uint8_t *output_b,
                           unsigned int buffer_length, uint8_t *key) {
  gcry_cipher_hd_t g_handle;
  gcry_cipher_open(&g_handle, GCRY_CIPHER_3DES, GCRY_CIPHER_MODE_ECB, 0);
  gcry_cipher_setkey(g_handle, key, des3_key_length);
  gcry_cipher_decrypt(g_handle, output_b, buffer_length, input_b, buffer_length);
  gcry_cipher_close(g_handle);
}


// Serpent
void gcrypt_encrypt_serpent (uint8_t *input_b, uint8_t *output_b,
                             unsigned int buffer_length, uint8_t *key) {
  gcry_cipher_hd_t g_handle;
  gcry_cipher_open(&g_handle, GCRY_CIPHER_SERPENT256, GCRY_CIPHER_MODE_ECB, 0);
  gcry_cipher_setkey(g_handle, key, serpent_key_length);
  gcry_cipher_encrypt(g_handle, output_b, buffer_length, input_b, buffer_length);
  gcry_cipher_close(g_handle);
}

void gcrypt_decrypt_serpent (uint8_t *input_b, uint8_t *output_b,
                             unsigned int buffer_length, uint8_t *key) {
  gcry_cipher_hd_t g_handle;
  gcry_cipher_open(&g_handle, GCRY_CIPHER_SERPENT256, GCRY_CIPHER_MODE_ECB, 0);
  gcry_cipher_setkey(g_handle, key, serpent_key_length);
  gcry_cipher_decrypt(g_handle, output_b, buffer_length, input_b, buffer_length);
  gcry_cipher_close(g_handle);
}


// Twofish
void gcrypt_encrypt_twofish (uint8_t *input_b, uint8_t *output_b,
                             unsigned int buffer_length, uint8_t *key) {
  gcry_cipher_hd_t g_handle;
  gcry_cipher_open(&g_handle, GCRY_CIPHER_TWOFISH, GCRY_CIPHER_MODE_ECB, 0);
  gcry_cipher_setkey(g_handle, key, twofish_key_length);
  gcry_cipher_encrypt(g_handle, output_b, buffer_length, input_b, buffer_length);
  gcry_cipher_close(g_handle);
}

void gcrypt_decrypt_twofish (uint8_t *input_b, uint8_t *output_b,
                             unsigned int buffer_length, uint8_t *key) {
  gcry_cipher_hd_t g_handle;
  gcry_cipher_open(&g_handle, GCRY_CIPHER_TWOFISH, GCRY_CIPHER_MODE_ECB, 0);
  gcry_cipher_setkey(g_handle, key, twofish_key_length);
  gcry_cipher_decrypt(g_handle, output_b, buffer_length, input_b, buffer_length);
  gcry_cipher_close(g_handle);
}


// Blowfish
void gcrypt_encrypt_blowfish (uint8_t *input_b, uint8_t *output_b,
                             unsigned int buffer_length, uint8_t *key) {
  gcry_cipher_hd_t g_handle;
  gcry_cipher_open(&g_handle, GCRY_CIPHER_BLOWFISH, GCRY_CIPHER_MODE_ECB, 0);
  gcry_cipher_setkey(g_handle, key, blowfish_key_length);
  gcry_cipher_encrypt(g_handle, output_b, buffer_length, input_b, buffer_length);
  gcry_cipher_close(g_handle);
}

void gcrypt_decrypt_blowfish (uint8_t *input_b, uint8_t *output_b,
                             unsigned int buffer_length, uint8_t *key) {
  gcry_cipher_hd_t g_handle;
  gcry_cipher_open(&g_handle, GCRY_CIPHER_BLOWFISH, GCRY_CIPHER_MODE_ECB, 0);
  gcry_cipher_setkey(g_handle, key, blowfish_key_length);
  gcry_cipher_decrypt(g_handle, output_b, buffer_length, input_b, buffer_length);
  gcry_cipher_close(g_handle);
}


// Cast5
void gcrypt_encrypt_cast5 (uint8_t *input_b, uint8_t *output_b,
                             unsigned int buffer_length, uint8_t *key) {
  gcry_cipher_hd_t g_handle;
  gcry_cipher_open(&g_handle, GCRY_CIPHER_CAST5, GCRY_CIPHER_MODE_ECB, 0);
  gcry_cipher_setkey(g_handle, key, cast5_key_length);
  gcry_cipher_encrypt(g_handle, output_b, buffer_length, input_b, buffer_length);
  gcry_cipher_close(g_handle);
}

void gcrypt_decrypt_cast5 (uint8_t *input_b, uint8_t *output_b,
                             unsigned int buffer_length, uint8_t *key) {
  gcry_cipher_hd_t g_handle;
  gcry_cipher_open(&g_handle, GCRY_CIPHER_CAST5, GCRY_CIPHER_MODE_ECB, 0);
  gcry_cipher_setkey(g_handle, key, cast5_key_length);
  gcry_cipher_decrypt(g_handle, output_b, buffer_length, input_b, buffer_length);
  gcry_cipher_close(g_handle);
}
