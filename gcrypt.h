#ifndef _GCRYPT_H_
#define _GCRYPT_H_

#include <unistd.h>
#include <stdint.h>

// AES
void gcrypt_encrypt_aes (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);

void gcrypt_decrypt_aes (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);


// DES
void gcrypt_encrypt_des (uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key);

void gcrypt_decrypt_des (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);


// 3DES
void gcrypt_encrypt_3des (uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key);

void gcrypt_decrypt_3des (uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key);


// Serpent
void gcrypt_encrypt_serpent (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);

void gcrypt_decrypt_serpent (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);


// Twofish
void gcrypt_encrypt_twofish (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);

void gcrypt_decrypt_twofish (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);


// Blowfish
void gcrypt_encrypt_blowfish (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);

void gcrypt_decrypt_blowfish (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);


// CAST5
void gcrypt_encrypt_cast5 (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);

void gcrypt_decrypt_cast5 (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);

#endif
