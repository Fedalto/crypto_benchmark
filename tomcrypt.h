#ifndef _TOMCRYPT_H_
#define _TOMCRYPT_H_

#include <unistd.h>
#include <stdint.h>

// AES
void tomcrypt_encrypt_aes (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);

void tomcrypt_decrypt_aes (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);


// DES
void tomcrypt_encrypt_des (uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key);

void tomcrypt_decrypt_des (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);


// 3DES
void tomcrypt_encrypt_3des (uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key);

void tomcrypt_decrypt_3des (uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key);


// Twofish
void tomcrypt_encrypt_twofish (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);

void tomcrypt_decrypt_twofish (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);


// Blowfish
void tomcrypt_encrypt_blowfish (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);

void tomcrypt_decrypt_blowfish (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);


// CAST5
void tomcrypt_encrypt_cast5 (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);

void tomcrypt_decrypt_cast5 (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);


#endif
