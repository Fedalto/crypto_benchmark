#include <unistd.h>
#include <stdint.h>

#include "mcrypt.h"
#include "benchmark.h"

int main () {
  const char *lib = "mcrypt";

  // AES
  void (*aes_encrypt)(uint8_t *, uint8_t *, unsigned int, uint8_t *) = &mcrypt_encrypt_aes;
  void (*aes_decrypt)(uint8_t *, uint8_t *, unsigned int, uint8_t *) = &mcrypt_decrypt_aes;
  benchmark(lib, "aes", "encrypt", aes_encrypt);
  benchmark(lib, "aes", "decrypt", aes_decrypt);

  // DES
  void (*des_encrypt)(uint8_t *, uint8_t *, unsigned int, uint8_t *) = &mcrypt_encrypt_des;
  void (*des_decrypt)(uint8_t *, uint8_t *, unsigned int, uint8_t *) = &mcrypt_decrypt_des;
  benchmark(lib, "des", "encrypt", des_encrypt);
  benchmark(lib, "des", "decrypt", des_decrypt);

  // 3DES
  void (*des3_encrypt)(uint8_t *, uint8_t *, unsigned int, uint8_t *) = &mcrypt_encrypt_3des;
  void (*des3_decrypt)(uint8_t *, uint8_t *, unsigned int, uint8_t *) = &mcrypt_decrypt_3des;
  benchmark(lib, "3des", "encrypt", des3_encrypt);
  benchmark(lib, "3des", "decrypt", des3_decrypt);

  // Serpent
  void (*serpent_encrypt)(uint8_t *, uint8_t *, unsigned int, uint8_t *) = &mcrypt_encrypt_serpent;
  void (*serpent_decrypt)(uint8_t *, uint8_t *, unsigned int, uint8_t *) = &mcrypt_decrypt_serpent;
  benchmark(lib, "serpent", "encrypt", serpent_encrypt);
  benchmark(lib, "serpent", "decrypt", serpent_decrypt);

  // Twofish
  void (*twofish_encrypt)(uint8_t *, uint8_t *, unsigned int, uint8_t *) = &mcrypt_encrypt_twofish;
  void (*twofish_decrypt)(uint8_t *, uint8_t *, unsigned int, uint8_t *) = &mcrypt_decrypt_twofish;
  benchmark(lib, "twofish", "encrypt", twofish_encrypt);
  benchmark(lib, "twofish", "decrypt", twofish_decrypt);

  // Blowfish
  void (*blowfish_encrypt)(uint8_t *, uint8_t *, unsigned int, uint8_t *) = &mcrypt_encrypt_blowfish;
  void (*blowfish_decrypt)(uint8_t *, uint8_t *, unsigned int, uint8_t *) = &mcrypt_decrypt_blowfish;
  benchmark(lib, "blowfish", "encrypt", blowfish_encrypt);
  benchmark(lib, "blowfish", "decrypt", blowfish_decrypt);

  // CAST5
  void (*cast5_encrypt)(uint8_t *, uint8_t *, unsigned int, uint8_t *) = &mcrypt_encrypt_cast5;
  void (*cast5_decrypt)(uint8_t *, uint8_t *, unsigned int, uint8_t *) = &mcrypt_decrypt_cast5;
  benchmark(lib, "cast5", "encrypt", cast5_encrypt);
  benchmark(lib, "cast5", "decrypt", cast5_decrypt);

  // CAST6
  void (*cast6_encrypt)(uint8_t *, uint8_t *, unsigned int, uint8_t *) = &mcrypt_encrypt_cast6;
  void (*cast6_decrypt)(uint8_t *, uint8_t *, unsigned int, uint8_t *) = &mcrypt_decrypt_cast6;
  benchmark(lib, "cast6", "encrypt", cast6_encrypt);
  benchmark(lib, "cast6", "decrypt", cast6_decrypt);

  return 0;
}
