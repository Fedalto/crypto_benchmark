CC = gcc
CFLAGS = $(shell libgcrypt-config --cflags) $(shell libmcrypt-config --cflags) -Wall
LIBS = $(shell libgcrypt-config --libs) $(shell libmcrypt-config --libs) -lssl -lcrypto -lrt -ltomcrypt -lbeecrypt -lgomp -lnettle
OBJS = benchmark.c logger.c key_length.c gcrypt.c mcrypt.c openssl.c tomcrypt.c beecrypt.c nettle.c
TARGETS = benchmark_gcrypt benchmark_mcrypt benchmark_openssl benchmark_tomcrypt benchmark_beecrypt benchmark_nettle

all: $(TARGETS)

benchmark_%: benchmark_%.c $(OBJS)
	$(CC) $< $(OBJS) $(CFLAGS) $(LIBS) -o $@

run: $(TARGETS) $(OBJS)
	@$(foreach target, $(TARGETS), ./$(target);)

clean:
	rm -f *.o $(TARGETS)
