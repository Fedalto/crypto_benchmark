#ifndef _KEY_LENGTH_H_
#define _KEY_LENGTH_H_

extern unsigned int aes_key_length,
                    des_key_length,
                    des3_key_length,
                    serpent_key_length,
                    twofish_key_length,
                    blowfish_key_length,
                    cast5_key_length,
                    cast6_key_length;

#endif
