#include <unistd.h>
#include <stdint.h>

// OpenSSL
#include <openssl/aes.h>
#include <openssl/des.h>
#include <openssl/blowfish.h>
#include <openssl/cast.h>

#include "key_length.h"


// AES
void openssl_encrypt_aes (uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key) {
  AES_KEY openssl_aes_key;
  AES_set_encrypt_key(key, aes_key_length*8, &openssl_aes_key);
  int i;
  for (i = 0; i < buffer_length; i += AES_BLOCK_SIZE)
    AES_encrypt(input_b+i, output_b+i, &openssl_aes_key);
}

void openssl_decrypt_aes (uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key) {
  AES_KEY openssl_aes_key;
  AES_set_decrypt_key(key, aes_key_length*8, &openssl_aes_key);
  int i;
  for (i = 0; i < buffer_length; i += AES_BLOCK_SIZE)
    AES_decrypt(input_b+i, output_b+i, &openssl_aes_key);
}

// DES
void openssl_encrypt_des (uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key) {
  DES_key_schedule openssl_des_key;
  DES_set_odd_parity((DES_cblock*)key);
  DES_set_key_checked((DES_cblock*)key, &openssl_des_key);

  int i;
  for (i = 0; i < buffer_length; i += 8)
    DES_ecb_encrypt((DES_cblock*)(input_b+i),
                    (DES_cblock*)(output_b+i),
                    &openssl_des_key,
                    DES_ENCRYPT);
}

void openssl_decrypt_des (uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key) {
  DES_key_schedule openssl_des_key;
  DES_set_odd_parity((DES_cblock*)key);
  DES_set_key_checked((DES_cblock*)key, &openssl_des_key);

  int i;
  for (i = 0; i < buffer_length; i += 8)
    DES_ecb_encrypt((DES_cblock*)(input_b+i),
                    (DES_cblock*)(output_b+i),
                    &openssl_des_key,
                    DES_DECRYPT);
}


// 3DES
void openssl_encrypt_3des (uint8_t *input_b, uint8_t *output_b,
                           unsigned int buffer_length, uint8_t *key) {
  DES_key_schedule openssl_des3_key1, openssl_des3_key2, openssl_des3_key3;
  DES_set_odd_parity((DES_cblock*)key);
  DES_set_odd_parity((DES_cblock*)(key + 8));
  DES_set_odd_parity((DES_cblock*)(key + 16));

  DES_set_key_checked((DES_cblock*)(key), &openssl_des3_key1);
  DES_set_key_checked((DES_cblock*)(key + 8), &openssl_des3_key2);
  DES_set_key_checked((DES_cblock*)(key + 16), &openssl_des3_key3);

  int i;
  for (i = 0; i < buffer_length; i += 8)
    DES_ecb3_encrypt((DES_cblock*)(input_b+i),
                     (DES_cblock*)(output_b+i),
                     &openssl_des3_key1,
                     &openssl_des3_key2,
                     &openssl_des3_key3,
                     DES_ENCRYPT);
}

void openssl_decrypt_3des (uint8_t *input_b, uint8_t *output_b,
                           unsigned int buffer_length, uint8_t *key) {
    DES_key_schedule openssl_des3_key1, openssl_des3_key2, openssl_des3_key3;
    DES_set_odd_parity((DES_cblock*)key);
    DES_set_odd_parity((DES_cblock*)(key + 8));
    DES_set_odd_parity((DES_cblock*)(key + 16));

    DES_set_key_checked((DES_cblock*)(key), &openssl_des3_key1);
    DES_set_key_checked((DES_cblock*)(key + 8), &openssl_des3_key2);
    DES_set_key_checked((DES_cblock*)(key + 16), &openssl_des3_key3);

    int i;
    for (i = 0; i < buffer_length; i += 8)
      DES_ecb3_encrypt((DES_cblock*)(input_b+i),
                       (DES_cblock*)(output_b+i),
                       &openssl_des3_key1,
                       &openssl_des3_key2,
                       &openssl_des3_key3,
                       DES_DECRYPT);
}


// Blowfish
void openssl_encrypt_blowfish (uint8_t *input_b, uint8_t *output_b,
                               unsigned int buffer_length, uint8_t *key) {
  BF_KEY openssl_blowfish_key;
  BF_set_key(&openssl_blowfish_key, blowfish_key_length, key);
  int i;
  for (i = 0; i < buffer_length; i += BF_BLOCK)
    BF_ecb_encrypt(input_b+i, output_b+i, &openssl_blowfish_key, BF_ENCRYPT);
}

void openssl_decrypt_blowfish (uint8_t *input_b, uint8_t *output_b,
                               unsigned int buffer_length, uint8_t *key) {
  BF_KEY openssl_blowfish_key;
  BF_set_key(&openssl_blowfish_key, blowfish_key_length, key);
  int i;
  for (i = 0; i < buffer_length; i += BF_BLOCK)
    BF_ecb_encrypt(input_b+i, output_b+i, &openssl_blowfish_key, BF_DECRYPT);
}


// CAST5
void openssl_encrypt_cast5 (uint8_t *input_b, uint8_t *output_b,
                               unsigned int buffer_length, uint8_t *key) {
  CAST_KEY openssl_cast5_key;
  CAST_set_key(&openssl_cast5_key, cast5_key_length, key);
  int i;
  for (i = 0; i < buffer_length; i += CAST_BLOCK)
    CAST_ecb_encrypt(input_b+i, output_b+i, &openssl_cast5_key, CAST_ENCRYPT);
}

void openssl_decrypt_cast5 (uint8_t *input_b, uint8_t *output_b,
                               unsigned int buffer_length, uint8_t *key) {
  CAST_KEY openssl_cast5_key;
  CAST_set_key(&openssl_cast5_key, cast5_key_length, key);
  int i;
  for (i = 0; i < buffer_length; i += CAST_BLOCK)
    CAST_ecb_encrypt(input_b+i, output_b+i, &openssl_cast5_key, CAST_DECRYPT);
}
