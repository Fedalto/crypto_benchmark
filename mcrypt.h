#ifndef _MCRYPT_H_
#define _MCRYPT_H_

#include <unistd.h>
#include <stdint.h>

// AES
void mcrypt_encrypt_aes (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);

void mcrypt_decrypt_aes (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);


// DES
void mcrypt_encrypt_des (uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key);

void mcrypt_decrypt_des (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);


// 3DES
void mcrypt_encrypt_3des (uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key);

void mcrypt_decrypt_3des (uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key);


// Serpent
void mcrypt_encrypt_serpent (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);

void mcrypt_decrypt_serpent (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);


// Twofish
void mcrypt_encrypt_twofish (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);

void mcrypt_decrypt_twofish (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);


// Blowfish
void mcrypt_encrypt_blowfish (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);

void mcrypt_decrypt_blowfish (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);


// CAST5
void mcrypt_encrypt_cast5 (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);

void mcrypt_decrypt_cast5 (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);


// CAST6
void mcrypt_encrypt_cast6 (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);

void mcrypt_decrypt_cast6 (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);

#endif
