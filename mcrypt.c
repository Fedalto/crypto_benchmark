#include <unistd.h>
#include <stdint.h>

// Libmcrypt
#include <mcrypt.h>

#include "key_length.h"


// Note: As MCrypt encrypt and decrypt in-place, the input_b will be replaced
// with the encrypted/decrypted text. This behaviour was not changed in this API
// because the benchmark should not consider the time to copy the input buffer.
// Make a copy of the input buffer if you want it unchanged.
//
// Again:
// The argument output_b will NOT be used.
// And the argument input_b WILL be modified.


// AES
void mcrypt_encrypt_aes(uint8_t *input_b, uint8_t *output_b,
                        unsigned int buffer_length, uint8_t *key) {
  MCRYPT m_handle = mcrypt_module_open("rijndael-128", NULL, "ecb", NULL);
  mcrypt_generic_init(m_handle, key, aes_key_length, NULL);
  mcrypt_generic(m_handle, input_b, buffer_length);
  mcrypt_generic_deinit(m_handle);
  mcrypt_module_close(m_handle);
}

void mcrypt_decrypt_aes(uint8_t *input_b, uint8_t *output_b,
                        unsigned int buffer_length, uint8_t *key) {
  MCRYPT m_handle = mcrypt_module_open("rijndael-128", NULL, "ecb", NULL);
  mcrypt_generic_init(m_handle, key, aes_key_length, NULL);
  mdecrypt_generic(m_handle, input_b, buffer_length);
  mcrypt_generic_deinit(m_handle);
  mcrypt_module_close(m_handle);
}


// DES
void mcrypt_encrypt_des (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key) {
  MCRYPT m_handle = mcrypt_module_open("des", NULL, "ecb", NULL);
  mcrypt_generic_init(m_handle, key, des_key_length, NULL);
  mcrypt_generic(m_handle, input_b, buffer_length);
  mcrypt_generic_deinit(m_handle);
  mcrypt_module_close(m_handle);
}

void mcrypt_decrypt_des (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key) {
  MCRYPT m_handle = mcrypt_module_open("des", NULL, "ecb", NULL);
  mcrypt_generic_init(m_handle, key, des_key_length, NULL);
  mdecrypt_generic(m_handle, input_b, buffer_length);
  mcrypt_generic_deinit(m_handle);
  mcrypt_module_close(m_handle);
}


// 3DES
void mcrypt_encrypt_3des (uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key) {
  MCRYPT m_handle = mcrypt_module_open(MCRYPT_3DES, NULL, "ecb", NULL);
  mcrypt_generic_init(m_handle, key, des3_key_length, NULL);
  mcrypt_generic(m_handle, input_b, buffer_length);
  mcrypt_generic_deinit(m_handle);
  mcrypt_module_close(m_handle);
}

void mcrypt_decrypt_3des (uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key) {
  MCRYPT m_handle = mcrypt_module_open(MCRYPT_3DES, NULL, "ecb", NULL);
  mcrypt_generic_init(m_handle, key, des3_key_length, NULL);
  mdecrypt_generic(m_handle, input_b, buffer_length);
  mcrypt_generic_deinit(m_handle);
  mcrypt_module_close(m_handle);
}


// Serpent
void mcrypt_encrypt_serpent(uint8_t *input_b, uint8_t *output_b,
                            unsigned int buffer_length, uint8_t *key) {
  MCRYPT m_handle = mcrypt_module_open(MCRYPT_SERPENT, NULL, "ecb", NULL);
  mcrypt_generic_init(m_handle, key, serpent_key_length, NULL);
  mcrypt_generic(m_handle, input_b, buffer_length);
  mcrypt_generic_deinit(m_handle);
  mcrypt_module_close(m_handle);
}

void mcrypt_decrypt_serpent(uint8_t *input_b, uint8_t *output_b,
                            unsigned int buffer_length, uint8_t *key) {
  MCRYPT m_handle = mcrypt_module_open(MCRYPT_SERPENT, NULL, "ecb", NULL);
  mcrypt_generic_init(m_handle, key, serpent_key_length, NULL);
  mdecrypt_generic(m_handle, input_b, buffer_length);
  mcrypt_generic_deinit(m_handle);
  mcrypt_module_close(m_handle);
}


// Twofish
void mcrypt_encrypt_twofish(uint8_t *input_b, uint8_t *output_b,
                            unsigned int buffer_length, uint8_t *key) {
  MCRYPT m_handle = mcrypt_module_open(MCRYPT_TWOFISH, NULL, "ecb", NULL);
  mcrypt_generic_init(m_handle, key, twofish_key_length, NULL);
  mcrypt_generic(m_handle, input_b, buffer_length);
  mcrypt_generic_deinit(m_handle);
  mcrypt_module_close(m_handle);
}

void mcrypt_decrypt_twofish(uint8_t *input_b, uint8_t *output_b,
                            unsigned int buffer_length, uint8_t *key) {
  MCRYPT m_handle = mcrypt_module_open(MCRYPT_TWOFISH, NULL, "ecb", NULL);
  mcrypt_generic_init(m_handle, key, twofish_key_length, NULL);
  mdecrypt_generic(m_handle, input_b, buffer_length);
  mcrypt_generic_deinit(m_handle);
  mcrypt_module_close(m_handle);
}


// Blowfish
void mcrypt_encrypt_blowfish(uint8_t *input_b, uint8_t *output_b,
                             unsigned int buffer_length, uint8_t *key) {
  MCRYPT m_handle = mcrypt_module_open(MCRYPT_BLOWFISH, NULL, "ecb", NULL);
  mcrypt_generic_init(m_handle, key, blowfish_key_length, NULL);
  mcrypt_generic(m_handle, input_b, buffer_length);
  mcrypt_generic_deinit(m_handle);
  mcrypt_module_close(m_handle);
}

void mcrypt_decrypt_blowfish(uint8_t *input_b, uint8_t *output_b,
                             unsigned int buffer_length, uint8_t *key) {
  MCRYPT m_handle = mcrypt_module_open(MCRYPT_BLOWFISH, NULL, "ecb", NULL);
  mcrypt_generic_init(m_handle, key, blowfish_key_length, NULL);
  mdecrypt_generic(m_handle, input_b, buffer_length);
  mcrypt_generic_deinit(m_handle);
  mcrypt_module_close(m_handle);
}


// CAST5
void mcrypt_encrypt_cast5(uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key) {
  MCRYPT m_handle = mcrypt_module_open(MCRYPT_CAST_128, NULL, "ecb", NULL);
  mcrypt_generic_init(m_handle, key, cast5_key_length, NULL);
  mcrypt_generic(m_handle, input_b, buffer_length);
  mcrypt_generic_deinit(m_handle);
  mcrypt_module_close(m_handle);
}

void mcrypt_decrypt_cast5(uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key) {
  MCRYPT m_handle = mcrypt_module_open(MCRYPT_CAST_128, NULL, "ecb", NULL);
  mcrypt_generic_init(m_handle, key, cast5_key_length, NULL);
  mdecrypt_generic(m_handle, input_b, buffer_length);
  mcrypt_generic_deinit(m_handle);
  mcrypt_module_close(m_handle);
}


// CAST6
void mcrypt_encrypt_cast6(uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key) {
  MCRYPT m_handle = mcrypt_module_open(MCRYPT_CAST_256, NULL, "ecb", NULL);
  mcrypt_generic_init(m_handle, key, cast6_key_length, NULL);
  mcrypt_generic(m_handle, input_b, buffer_length);
  mcrypt_generic_deinit(m_handle);
  mcrypt_module_close(m_handle);
}

void mcrypt_decrypt_cast6(uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key) {
  MCRYPT m_handle = mcrypt_module_open(MCRYPT_CAST_256, NULL, "ecb", NULL);
  mcrypt_generic_init(m_handle, key, cast6_key_length, NULL);
  mdecrypt_generic(m_handle, input_b, buffer_length);
  mcrypt_generic_deinit(m_handle);
  mcrypt_module_close(m_handle);
}
