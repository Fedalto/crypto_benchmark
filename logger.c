#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <errno.h>

#include "logger.h"

const char * log_dir = "log/";


logger_t logger_init(const char * lib, const char * cipher, const char * mode) {
  if (access(log_dir, F_OK))
    mkdir(log_dir, 0777);

  char * log_file = malloc(sizeof(char)*50);
  snprintf(log_file, sizeof(char)*50,
           "%s/%s-%s-%s.log",
           log_dir, lib, cipher, mode);

  logger_t l = fopen(log_file, "w");
  if (l == NULL)
    perror("Error opening log file");

  free(log_file);
  return l;
}

void logger_close(logger_t l) {
  fclose(l);
}
