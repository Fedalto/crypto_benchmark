#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <time.h>
#include <unistd.h>
#include <limits.h>

// Utility functions and definitions
#include "benchmark.h"
#include "logger.h"

// Constants
const unsigned int initial_buffer_length = 16;    // = 16 B
const unsigned int max_buffer_length = 1024*1024; // = 1  MB
const unsigned int repeats_max = 1; // TODO: Change this in real test
// const unsigned int repeats_max = 10000; // TODO: Change this in real test

#define SEC *1000000000 // 10^9

// Buffer utilities
uint8_t * generate_random_buffer(unsigned int length){
  uint8_t *buf = (uint8_t *) malloc(sizeof(uint8_t)*length);

  uint i;
  for (i = 0; i < length; i++)
    buf[i] = rand();

  return buf;
}


// Timer functions
typedef struct timespec benchmark_timer_t;

// Returns a benchmark_timer_t struct for stop_timer.
benchmark_timer_t start_timer () {
  benchmark_timer_t t;
  clock_gettime(CLOCK_MONOTONIC, &t);
  return t;
}

// Return the number of nanoseconds since start_timer.
uint64_t stop_timer(benchmark_timer_t start) {
  benchmark_timer_t stop;
  clock_gettime(CLOCK_MONOTONIC, &stop);

  uint64_t nano_start = start.tv_sec SEC + start.tv_nsec;
  uint64_t nano_stop = stop.tv_sec SEC + stop.tv_nsec;

  return nano_stop - nano_start;
}

// Benchmark function
// lib, cipher and mode are strings describing which library is being use (gcrypt,
// mcrypt), which cipher (aes, des) and mode (encrypt or decrypt). For logging purpose only.
// fp is a pointer to the function to be benchmarked. It receives an input buffer,
// output buffer, length of those buffers and the key.
void benchmark (const char *lib, const char *cipher, const char *mode,
                void (*fp)(uint8_t *, uint8_t *, unsigned int, uint8_t *)) {

  // Progress feedback
  printf("Benchmarking %10s / %-10s: \t", lib, cipher);
  fflush(stdout);

  // set random seed
  srand(time(NULL));

  logger_t logger = logger_init(lib, cipher, mode);

  uint8_t *key = generate_random_buffer(256/8);
  uint8_t *input_b = generate_random_buffer(max_buffer_length);

  unsigned int buffer_length;
  for (buffer_length = initial_buffer_length;
       buffer_length <= max_buffer_length;
       buffer_length *= 2) {

    uint8_t output_b[buffer_length];
    uint64_t results_sum = 0;
    int repeats;

    uint64_t result, max = 0, min = ULONG_MAX;

    // Warm-up: First runs will get a page fault.
    // We shouldn`t consider these runs.
    (*fp) (input_b, output_b, buffer_length, key);
    (*fp) (input_b, output_b, buffer_length, key);
    (*fp) (input_b, output_b, buffer_length, key);

    for (repeats = 1; repeats <= repeats_max; repeats++){

      benchmark_timer_t t;

      // Always consider the first couple runs.
      // After that, if a result is big enough, ignore and repeat it.
      if (repeats <= 2)
      {
        // Start Benchmark //
        t = start_timer();

        (*fp) (input_b, output_b, buffer_length, key);

        // Stop Benchmark //
        result = stop_timer(t);
      } else {
        do {
          // Start Benchmark //
          t = start_timer();

          (*fp) (input_b, output_b, buffer_length, key);

          // Stop Benchmark //
          result = stop_timer(t);
        } while (result > 1.3 * (results_sum/(repeats-1)));
      }


      results_sum += result;

      if (result < min)
        min = result;
      if (result > max)
        max = result;
    }

    // Progress feedback
    printf("*");
    fflush(stdout);

    // Log the results in a file.
    fprintf(logger, "%u\t%ld\t%ld\t%ld\n",
            buffer_length,
            results_sum/repeats_max,
            min,
            max);
  }

  // Progress feedback
  printf("\n");
  fflush(stdout);

  // Clean up
  free(key);
  free(input_b);
  logger_close(logger);
}
