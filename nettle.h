#ifndef _NETTLE_H_
#define _NETTLE_H_

#include <unistd.h>
#include <stdint.h>

// AES
void nettle_encrypt_aes (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);

void nettle_decrypt_aes (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);


// DES
void nettle_encrypt_des (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);

void nettle_decrypt_des (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);


// 3DES
void nettle_encrypt_3des (uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key);

void nettle_decrypt_3des (uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key);


// Serpent
void nettle_encrypt_serpent (uint8_t *input_b, uint8_t *output_b,
                             unsigned int buffer_length, uint8_t *key);

void nettle_decrypt_serpent (uint8_t *input_b, uint8_t *output_b,
                             unsigned int buffer_length, uint8_t *key);


// Twofish
void nettle_encrypt_twofish (uint8_t *input_b, uint8_t *output_b,
                             unsigned int buffer_length, uint8_t *key);

void nettle_decrypt_twofish (uint8_t *input_b, uint8_t *output_b,
                             unsigned int buffer_length, uint8_t *key);


// Blowfish
void nettle_encrypt_blowfish (uint8_t *input_b, uint8_t *output_b,
                              unsigned int buffer_length, uint8_t *key);

void nettle_decrypt_blowfish (uint8_t *input_b, uint8_t *output_b,
                              unsigned int buffer_length, uint8_t *key);


// CAST5
void nettle_encrypt_cast5 (uint8_t *input_b, uint8_t *output_b,
                           unsigned int buffer_length, uint8_t *key);

void nettle_decrypt_cast5 (uint8_t *input_b, uint8_t *output_b,
                           unsigned int buffer_length, uint8_t *key);

#endif
