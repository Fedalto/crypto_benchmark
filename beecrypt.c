#include <unistd.h>
#include <stdint.h>

#include <beecrypt/aes.h>
#include <beecrypt/blowfish.h>

#include "key_length.h"

// AES
void beecrypt_encrypt_aes (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key) {
  aesParam ap;
  aesSetup(&ap, key, aes_key_length*8, ENCRYPT);
  int i;
  for (i = 0; i < buffer_length; i += 16)
    aesEncrypt(&ap, (uint32_t*)(output_b+i), (uint32_t*)(input_b+i));
}

void beecrypt_decrypt_aes (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key) {
  aesParam ap;
  aesSetup(&ap, key, aes_key_length*8, DECRYPT);
  int i;
  for (i = 0; i < buffer_length; i += 16)
    aesDecrypt(&ap, (uint32_t*)(output_b+i), (uint32_t*)(input_b+i));
}


// Blowfish
void beecrypt_encrypt_blowfish (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key){
  blowfishParam bp;
  blowfishSetup (&bp, key, blowfish_key_length*8, ENCRYPT);
  int i;
  for (i = 0; i < buffer_length; i += 8)
    blowfishEncrypt(&bp, (uint32_t*)(output_b+i), (uint32_t*)(input_b+i));
}

void beecrypt_decrypt_blowfish (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key){
  blowfishParam bp;
  blowfishSetup (&bp, key, blowfish_key_length*8, DECRYPT);
  int i;
  for (i = 0; i < buffer_length; i += 8)
    blowfishDecrypt(&bp, (uint32_t*)(output_b+i), (uint32_t*)(input_b+i));
}
