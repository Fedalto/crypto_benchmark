#include <unistd.h>
#include <stdint.h>

#include <nettle/aes.h>
#include <nettle/des.h>
#include <nettle/blowfish.h>
#include <nettle/cast128.h>
#include <nettle/serpent.h>
#include <nettle/twofish.h>

#include "key_length.h"


// AES
void nettle_encrypt_aes (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key) {
  struct aes_ctx ctx;
  aes_set_encrypt_key (&ctx, aes_key_length, key);
  aes_encrypt (&ctx, buffer_length, output_b, input_b);
}

void nettle_decrypt_aes (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key) {
  struct aes_ctx ctx;
  aes_set_decrypt_key (&ctx, aes_key_length, key);
  aes_decrypt (&ctx, buffer_length, output_b, input_b);
}


// DES
void nettle_encrypt_des (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key) {
  struct des_ctx ctx;
  uint8_t des_key[des_key_length];
  des_fix_parity(des_key_length, des_key, key);
  des_set_key (&ctx, des_key);
  des_encrypt (&ctx, buffer_length, output_b, input_b);
}

void nettle_decrypt_des (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key) {
  struct des_ctx ctx;
  uint8_t des_key[des_key_length];
  des_fix_parity(des_key_length, des_key, key);
  des_set_key (&ctx, des_key);
  des_decrypt (&ctx, buffer_length, output_b, input_b);
}


// 3DES
void nettle_encrypt_3des (uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key) {
  struct des3_ctx ctx;
  uint8_t des3_key[des3_key_length];
  des_fix_parity(des3_key_length, des3_key, key);
  des3_set_key (&ctx, des3_key);
  des3_encrypt (&ctx, buffer_length, output_b, input_b);
}

void nettle_decrypt_3des (uint8_t *input_b, uint8_t *output_b,
                          unsigned int buffer_length, uint8_t *key) {
  struct des3_ctx ctx;
  uint8_t des3_key[des3_key_length];
  des_fix_parity(des3_key_length, des3_key, key);
  des3_set_key (&ctx, des3_key);
  des3_decrypt (&ctx, buffer_length, output_b, input_b);
}


// Serpent
void nettle_encrypt_serpent (uint8_t *input_b, uint8_t *output_b,
                             unsigned int buffer_length, uint8_t *key) {
  // The Serpent implementation in Nettle is bugged.
  // The serpent_encrypt function is incorrectly reversing the byte order from the output
  // For more info see http://lists.lysator.liu.se/pipermail/nettle-bugs/2011/002188.html
  struct serpent_ctx ctx;
  serpent_set_key (&ctx, serpent_key_length, key);
  serpent_encrypt (&ctx, buffer_length, output_b, input_b);
}

void nettle_decrypt_serpent (uint8_t *input_b, uint8_t *output_b,
                             unsigned int buffer_length, uint8_t *key) {
  struct serpent_ctx ctx;
  serpent_set_key (&ctx, serpent_key_length, key);
  serpent_decrypt (&ctx, buffer_length, output_b, input_b);
}


// Twofish
void nettle_encrypt_twofish (uint8_t *input_b, uint8_t *output_b,
                             unsigned int buffer_length, uint8_t *key) {
  struct twofish_ctx ctx;
  twofish_set_key (&ctx, twofish_key_length, key);
  twofish_encrypt (&ctx, buffer_length, output_b, input_b);
}

void nettle_decrypt_twofish (uint8_t *input_b, uint8_t *output_b,
                             unsigned int buffer_length, uint8_t *key) {
  struct twofish_ctx ctx;
  twofish_set_key (&ctx, twofish_key_length, key);
  twofish_decrypt (&ctx, buffer_length, output_b, input_b);
}


// Blowfish
void nettle_encrypt_blowfish (uint8_t *input_b, uint8_t *output_b,
                              unsigned int buffer_length, uint8_t *key) {
  struct blowfish_ctx ctx;
  blowfish_set_key(&ctx, blowfish_key_length, key);
  blowfish_encrypt (&ctx, buffer_length, output_b, input_b);
}

void nettle_decrypt_blowfish (uint8_t *input_b, uint8_t *output_b,
                              unsigned int buffer_length, uint8_t *key) {
  struct blowfish_ctx ctx;
  blowfish_set_key (&ctx, blowfish_key_length, key);
  blowfish_decrypt (&ctx, buffer_length, output_b, input_b);
}


// Cast5
void nettle_encrypt_cast5 (uint8_t *input_b, uint8_t *output_b,
                           unsigned int buffer_length, uint8_t *key) {
  struct cast128_ctx ctx;
  cast128_set_key (&ctx, cast5_key_length, key);
  cast128_encrypt (&ctx, buffer_length, output_b, input_b);
}

void nettle_decrypt_cast5 (uint8_t *input_b, uint8_t *output_b,
                           unsigned int buffer_length, uint8_t *key) {
  struct cast128_ctx ctx;
  cast128_set_key (&ctx, cast5_key_length, key);
  cast128_decrypt (&ctx, buffer_length, output_b, input_b);
}
