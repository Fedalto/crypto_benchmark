#ifndef _LOGGER_H_
#define _LOGGER_H_

#include "stdio.h"

typedef FILE* logger_t;

logger_t logger_init(const char * lib, const char * cipher, const char * mode);

void logger_close(logger_t l);

#endif
