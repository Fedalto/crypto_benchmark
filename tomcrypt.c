#include <unistd.h>
#include <stdint.h>

#include <tomcrypt.h>

#include "key_length.h"

// AES
void tomcrypt_encrypt_aes (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key) {
  symmetric_key tomcrypt_aes_key;
  aes_setup(key, aes_key_length, 0, &tomcrypt_aes_key);
  int i;
  for (i = 0; i < buffer_length; i+= 16)
    aes_ecb_encrypt(input_b+i, output_b+i, &tomcrypt_aes_key);
  aes_done(&tomcrypt_aes_key);
}

void tomcrypt_decrypt_aes (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key) {
  symmetric_key tomcrypt_aes_key;
  aes_setup(key, aes_key_length, 0, &tomcrypt_aes_key);
  int i;
  for (i = 0; i < buffer_length; i+= 16)
    aes_ecb_decrypt(input_b+i, output_b+i, &tomcrypt_aes_key);
  aes_done(&tomcrypt_aes_key);
}


// DES
void tomcrypt_encrypt_des (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key) {
  symmetric_key tomcrypt_des_key;
  des_setup(key, des_key_length, 0, &tomcrypt_des_key);
  int i;
  for (i = 0; i < buffer_length; i+= 8)
    des_ecb_encrypt(input_b+i, output_b+i, &tomcrypt_des_key);
  des_done(&tomcrypt_des_key);
}

void tomcrypt_decrypt_des (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key) {
  symmetric_key tomcrypt_des_key;
  des_setup(key, des_key_length, 0, &tomcrypt_des_key);
  int i;
  for (i = 0; i < buffer_length; i+= 8)
    des_ecb_decrypt(input_b+i, output_b+i, &tomcrypt_des_key);
  des_done(&tomcrypt_des_key);
}


// 3DES
void tomcrypt_encrypt_3des (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key) {
  symmetric_key tomcrypt_3des_key;
  des3_setup(key, des3_key_length, 0, &tomcrypt_3des_key);
  int i;
  for (i = 0; i < buffer_length; i+= 8)
    des3_ecb_encrypt(input_b+i, output_b+i, &tomcrypt_3des_key);
  des3_done(&tomcrypt_3des_key);
}

void tomcrypt_decrypt_3des (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key) {
  symmetric_key tomcrypt_3des_key;
  des3_setup(key, des3_key_length, 0, &tomcrypt_3des_key);
  int i;
  for (i = 0; i < buffer_length; i+= 8)
    des3_ecb_decrypt(input_b+i, output_b+i, &tomcrypt_3des_key);
  des3_done(&tomcrypt_3des_key);
}


// Twofish
void tomcrypt_encrypt_twofish (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key){
  symmetric_key tomcrypt_twofish_key;
  twofish_setup(key, twofish_key_length, 0, &tomcrypt_twofish_key);
  int i;
  for (i = 0; i < buffer_length; i+= 16)
    twofish_ecb_encrypt(input_b+i, output_b+i, &tomcrypt_twofish_key);
  twofish_done(&tomcrypt_twofish_key);
}

void tomcrypt_decrypt_twofish (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key){
  symmetric_key tomcrypt_twofish_key;
  twofish_setup(key, twofish_key_length, 0, &tomcrypt_twofish_key);
  int i;
  for (i = 0; i < buffer_length; i+= 16)
    twofish_ecb_decrypt(input_b+i, output_b+i, &tomcrypt_twofish_key);
  twofish_done(&tomcrypt_twofish_key);
}


// Blowfish
void tomcrypt_encrypt_blowfish (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key){
  symmetric_key tomcrypt_blowfish_key;
  blowfish_setup(key, blowfish_key_length, 0, &tomcrypt_blowfish_key);
  int i;
  for (i = 0; i < buffer_length; i+= 8)
    blowfish_ecb_encrypt(input_b+i, output_b+i, &tomcrypt_blowfish_key);
  blowfish_done(&tomcrypt_blowfish_key);
}

void tomcrypt_decrypt_blowfish (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key){
  symmetric_key tomcrypt_blowfish_key;
  blowfish_setup(key, blowfish_key_length, 0, &tomcrypt_blowfish_key);
  int i;
  for (i = 0; i < buffer_length; i+= 8)
    blowfish_ecb_decrypt(input_b+i, output_b+i, &tomcrypt_blowfish_key);
  blowfish_done(&tomcrypt_blowfish_key);
}


// CAST5
void tomcrypt_encrypt_cast5 (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key){
  symmetric_key tomcrypt_cast5_key;
  cast5_setup(key, cast5_key_length, 0, &tomcrypt_cast5_key);
  int i;
  for (i = 0; i < buffer_length; i+= 8)
    cast5_ecb_encrypt(input_b+i, output_b+i, &tomcrypt_cast5_key);
  cast5_done(&tomcrypt_cast5_key);
}

void tomcrypt_decrypt_cast5 (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key){
  symmetric_key tomcrypt_cast5_key;
  cast5_setup(key, cast5_key_length, 0, &tomcrypt_cast5_key);
  int i;
  for (i = 0; i < buffer_length; i+= 8)
    cast5_ecb_decrypt(input_b+i, output_b+i, &tomcrypt_cast5_key);
  cast5_done(&tomcrypt_cast5_key);
}
