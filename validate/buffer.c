#include <stdint.h>
#include <stdlib.h>


const unsigned int buffer_length = 1024*1024; // = 1Mb


uint8_t * generate_random_buffer(unsigned int length){
  uint8_t *buf = (uint8_t *) malloc(sizeof(uint8_t)*length);

  int i;
  for (i = 0; i < length; i++)
    buf[i] = rand();

  return buf;
}


void copy_buffer(uint8_t *in, uint8_t *out, unsigned int length){
  int i;
  for (i = 0; i < length; i++)
    out[i] = in[i];
}

int compare_buffers(uint8_t *b1, uint8_t *b2, unsigned int length){
  int i;
  for (i = 0; i < length; i++)
    if (b1[i] != b2[i])
      return 1;
  return 0;
}
