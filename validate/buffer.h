#ifndef _BUFFER_H_
#define _BUFFER_H_

// Debug
#define DEBUG_LEVEL 5
#define DEBUG(level, ...) if (DEBUG_LEVEL >= level) printf(__VA_ARGS__);

// Constants
extern const unsigned int buffer_length;


uint8_t * generate_random_buffer(unsigned int length);

void copy_buffer(uint8_t *in, uint8_t *out, unsigned int length);

int compare_buffers(uint8_t *b1, uint8_t *b2, unsigned int length);

#endif
