#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

// Crypt libs
#include "../gcrypt.h"
#include "../mcrypt.h"
#include "../nettle.h"


#include "../key_length.h"
#include "buffer.h"


int main () {
  // set random seed
  srand(time(NULL));

  DEBUG(0, "Validating Serpent");

  uint8_t *original_data = generate_random_buffer(buffer_length);
  uint8_t *key = generate_random_buffer(serpent_key_length);

  //
  // Encrypt
  //

  // ligbcrypt
  uint8_t libgcrypt_b[buffer_length];
  gcrypt_encrypt_serpent (original_data, libgcrypt_b, buffer_length, key);

  // libmcrypt
  uint8_t libmcrypt_b[buffer_length];
  copy_buffer(original_data, libmcrypt_b, buffer_length);
  mcrypt_encrypt_serpent (libmcrypt_b, libmcrypt_b, buffer_length, key);

  // Nettle
  uint8_t nettle_b[buffer_length];
  nettle_encrypt_serpent (original_data, nettle_b, buffer_length, key);


  // Compare buffers
  if( compare_buffers(libgcrypt_b, libmcrypt_b, buffer_length)) {
    DEBUG(0, ": FAIL! \n");
    return 0;
  }

  //
  // Decrypt
  //

  // libgcrypt
  gcrypt_decrypt_serpent (libgcrypt_b, libgcrypt_b, buffer_length, key);

  // libmcrypt
  mcrypt_decrypt_serpent (libmcrypt_b, libmcrypt_b, buffer_length, key);

  // Nettle
  nettle_decrypt_serpent (nettle_b, nettle_b, buffer_length, key);


  // Compare with the original data
  if (compare_buffers(original_data, libgcrypt_b, buffer_length) +
      compare_buffers(original_data, libmcrypt_b, buffer_length) +
      compare_buffers(original_data, nettle_b, buffer_length)){
    DEBUG(0, ": FAIL! \n");
    return 0;
  }

  DEBUG(0, ": OK\n");

  free(original_data);
  free(key);

  return 0;
}
