#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

// Crypt libs
#include "../gcrypt.h"
#include "../mcrypt.h"
#include "../openssl.h"
#include "../tomcrypt.h"
#include "../nettle.h"


#include "../key_length.h"
#include "buffer.h"


int main () {
  // set random seed
  srand(time(NULL));

  DEBUG(0, "Validating 3DES");

  uint8_t *original_data = generate_random_buffer(buffer_length);
  uint8_t *key = generate_random_buffer(des3_key_length);

  //
  // Encrypt
  //

  // ligbcrypt
  uint8_t libgcrypt_b[buffer_length];
  gcrypt_encrypt_3des (original_data, libgcrypt_b, buffer_length, key);

  // libmcrypt
  uint8_t libmcrypt_b[buffer_length];
  copy_buffer(original_data, libmcrypt_b, buffer_length);
  mcrypt_encrypt_3des (libmcrypt_b, libmcrypt_b, buffer_length, key);

  // OpenSSL
  uint8_t openssl_b[buffer_length];
  openssl_encrypt_3des (original_data, openssl_b, buffer_length, key);

  // Tomcrypt
  uint8_t tomcrypt_b[buffer_length];
  openssl_encrypt_3des (original_data, tomcrypt_b, buffer_length, key);

  // Nettle
  uint8_t nettle_b[buffer_length];
  nettle_encrypt_3des (original_data, nettle_b, buffer_length, key);


  // Compare buffers
  if( compare_buffers(libgcrypt_b, libmcrypt_b, buffer_length) +
      compare_buffers(libgcrypt_b, openssl_b,   buffer_length) +
      compare_buffers(libgcrypt_b, tomcrypt_b,  buffer_length) +
      compare_buffers(libgcrypt_b, nettle_b,    buffer_length)) {
    DEBUG(0, ": FAIL! \n");
    return 0;
  }

  //
  // Decrypt
  //

  // libgcrypt
  gcrypt_decrypt_3des (libgcrypt_b, libgcrypt_b, buffer_length, key);

  // libmcrypt
  mcrypt_decrypt_3des (libmcrypt_b, libmcrypt_b, buffer_length, key);

  // OpenSSL
  openssl_decrypt_3des (openssl_b, openssl_b, buffer_length, key);

  // Tomcrypt
  openssl_decrypt_3des (tomcrypt_b, tomcrypt_b, buffer_length, key);

  // Nettle
  nettle_decrypt_3des (nettle_b, nettle_b, buffer_length, key);


  // Compare with the original data
  if (compare_buffers(original_data, libgcrypt_b, buffer_length) +
      compare_buffers(original_data, libmcrypt_b, buffer_length) +
      compare_buffers(original_data, openssl_b,   buffer_length) +
      compare_buffers(original_data, tomcrypt_b,  buffer_length) +
      compare_buffers(original_data, nettle_b,    buffer_length)){
    DEBUG(0, ": FAIL! \n");
    return 0;
  }

  DEBUG(0, ": OK\n");

  free(original_data);
  free(key);

  return 0;
}
