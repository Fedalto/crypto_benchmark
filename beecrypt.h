#ifndef _BEECRYPT_H_
#define _BEECRYPT_H_

#include <unistd.h>
#include <stdint.h>

// AES
void beecrypt_encrypt_aes (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);

void beecrypt_decrypt_aes (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);


// Blowfish
void beecrypt_encrypt_blowfish (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);

void beecrypt_decrypt_blowfish (uint8_t *input_b, uint8_t *output_b,
                         unsigned int buffer_length, uint8_t *key);


#endif
