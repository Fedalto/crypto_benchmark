const unsigned int aes_key_length = 256/8;
const unsigned int des_key_length = 64/8; // DES uses 56 bits
const unsigned int des3_key_length = 192/8; // 3DES uses 168 bits
const unsigned int serpent_key_length = 256/8;
const unsigned int twofish_key_length = 256/8;
const unsigned int blowfish_key_length = 128/8;
const unsigned int cast5_key_length = 128/8;
const unsigned int cast6_key_length = 256/8;
