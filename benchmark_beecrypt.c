#include <unistd.h>
#include <stdint.h>

#include "beecrypt.h"
#include "benchmark.h"

int main () {
  const char *lib = "beecrypt";

  // AES
  void (*aes_encrypt)(uint8_t *, uint8_t *, unsigned int, uint8_t *) = &beecrypt_encrypt_aes;
  void (*aes_decrypt)(uint8_t *, uint8_t *, unsigned int, uint8_t *) = &beecrypt_decrypt_aes;
  benchmark(lib, "aes", "encrypt", aes_encrypt);
  benchmark(lib, "aes", "decrypt", aes_decrypt);

  // Blowfish
  void (*blowfish_encrypt)(uint8_t *, uint8_t *, unsigned int, uint8_t *) = &beecrypt_encrypt_blowfish;
  void (*blowfish_decrypt)(uint8_t *, uint8_t *, unsigned int, uint8_t *) = &beecrypt_decrypt_blowfish;
  benchmark(lib, "blowfish", "encrypt", blowfish_encrypt);
  benchmark(lib, "blowfish", "decrypt", blowfish_decrypt);

  return 0;
}
