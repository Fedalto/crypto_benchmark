#ifndef _BENCHMARK_H_
#define _BENCHMARK_H_

#include <stdint.h>
#include <unistd.h>

void benchmark (const char *lib, const char *cipher, const char *mode,
                void (*fp)(uint8_t *, uint8_t *, unsigned int, uint8_t *));

#endif
